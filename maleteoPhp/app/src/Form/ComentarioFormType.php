<?php

namespace App\Form;

use App\Entity\Comentario;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use \Symfony\Component\Form\FormBuilderInterface;
use \Symfony\Component\OptionsResolver\OptionsResolver;

class ComentarioFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('texto');
        $builder->add('propietario');
        $builder->add('ubicacion');
        $builder->add('likes', ChoiceType::class, [
            'choices'=>[
                '1'=>1,
                '2'=>2,
                '3'=>3,
                '4'=>4
            ]
        ]);
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                "data_class" => Comentario::class
            ]
        );
    }
}
