<?php
namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Comentario;
use App\Form\ComentarioFormType;
use Symfony\Component\HttpFoundation\Request;

class ComentarioController extends AbstractController{
    /**
     * @Route("listarC", name="listarC")
     */
    public function listar(EntityManagerInterface $m){
        $repo=$m->getRepository(Comentario::class);
        $coments= $repo->findAll();

        return $this->render('tablaC.html.twig',[
            'coments'=> $coments
        ]);

    }
    /**
     * @Route("/insertarC", name="insertarComentario")
     */
    public function insertarC(Request $req, EntityManagerInterface $em){
        /*creamos form*/
        $formu=$this->createForm(ComentarioFormType::class);
            /*rellena sus campos cuando venga por la request*/$formu->handleRequest($req);/**/
            if($formu->isSubmitted() && $formu->isValid()){
                /*mostramos data con interrurupcion y redirigimos*/
                $comentario=$formu->getData();
                //dd($comentario);
                /*esto devolveria array pero lo linkearemos a un objeto
                    para acer ke el form es de la clase y devuelva la entidad directo magic*/
                    /*para no acerlo a mano hacemos el configureOptions  del abstracttype */
                    $em->persist($comentario);$em-> flush();


                return $this->redirectToRoute('listarC');/*nombre*/
            }
        /*renderizamos el form*/
        return $this->render(
              'newComentario.html.twig',
              [
                'formulario'=> $formu->createView()
              ]
        );
    }
    /*para editar es lo mismo pero recogiendo id por ruta dinamica y pasandole
    la clase;
    ojo al dato como se le pasa la id por el path con el valor de comentario
    */
    /**
     * @Route("/editComent/{id}", name="editC")
     */
    public function editarComent(Comentario $com, Request $req, EntityManagerInterface $em){
        /*creamos form, si magicamente le pasamos objeto de esa clase lo reellena con eso*/
        $formu=$this->createForm(ComentarioFormType::class, $com);
                $formu->handleRequest($req);/**/
            if($formu->isSubmitted() && $formu->isValid()){
                
                $comentario=$formu->getData();

                    $em->persist($comentario);$em-> flush();


                return $this->redirectToRoute('listarC');/*nombre*/
            }
        /*renderizamos el form, con los datos*/
        return $this->render(
              'newComentario.html.twig',
              [
                'formulario'=> $formu->createView()
              ]
        );
    }
}