<?php
namespace  App\Controller;

use App\Entity\Usuario;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class MainController extends AbstractController{
    /**
     * @Route("/", name="homepage")
     */
    public function index(){
        
        return $this->render('base.html.twig',[
            'dato' => "UselessShiat"
            ]);
    }
    /**
     * @Route("/insertar", name="insertarU")
     */
    public function insertar(EntityManagerInterface $em){
        $usuario= new Usuario();
        $usuario->setNombre("Jones");
        $usuario->setEmail("asdasdasdlk@prueba.com");

        $em->persist($usuario);
        $em->flush();
        return new Response("usuario insertado ok");
    }
    /**
     * @Route("/listarU", name="listaU")
     */
    public function listarU(EntityManagerInterface $m){
        $repo=$m->getRepository(Usuario::class);
        $usuarios=$repo->findAll();

        return $this->render(
            'tablaU.html.twig',
            ['usuarios' => $usuarios]
        );
    }

}